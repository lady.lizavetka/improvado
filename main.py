import requests
import names
import json

a = int(input())

keys = ["name", "age", "count"]
results = open('results.txt', 'w')
results.write(str(keys) + '\n')

i = 0
for i in range(a):
    name = names.get_first_name()
    r = requests.get("https://api.agify.io/?name=" + name)
    decoded = json.loads(r.text)
    d = dict(zip(keys, [decoded['name'], decoded['age'], decoded['count']]))

    values = str([*d.values()])
    results.write(values + '\n')

    i += 1

results.close()
